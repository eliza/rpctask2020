cmsrel CMSSW_11_1_2_patch2

cd CMSSW_11_1_2_patch2/src/

cmsenv

git clone https://gitlab.cern.ch/eliza/rpctask2020.git MuDPGAnalysis/MuonDPGNtuples


scram b -j 5

cd MuDPGAnalysis/MuonDPGNtuples/test/

cmsRun muDpgNtuples_cfg.py
